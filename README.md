# README #

This is a simple wrapper for installing brew and brew casks. It is really oriented to fresh installs for initial setups--this includes the excellent brew cask update scripts, so that wheel is already invented. There's a very simple manifest system, you make a folder and just list the casks and programs you want to install, and declare the folder with -m