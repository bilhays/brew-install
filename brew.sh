#!/bin/bash

#
# Author:
# hays 18 Oct 2017
#
version="0.1";
# Copyright 2017 by bil hays

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# CAVEAT: Please do not assume that I know what I am doing. Often I don't. Do
# not depend on me or my code to do the expected or desired, we are liable to
# disappoint you. We take no responsibilty for anything the might happen
# to you or those around you should you use this software.
#




PROG="${0##*/}";
#PROG="${PROG%.new}";
#PROG="${PROG%.sh}";
#PROG="${PROG%.bash}";


BREW="/usr/local/bin/brew";
version="0.8";

BREWLOG="./brew.log";

# Evidently Travis doesn't like to hang around and press return to
# continue, setting this lets the installer whiz right through
TRAVIS=1
export TRAVIS

function usage()
   {
cat <<EOF
v version
m Folder with manifests for this install
h usage

EOF
exit;
    }

function check_errm
   {
   if  [[ ${?} != "0" ]]
      then
      echo "${1}";
      exit ${2};
      fi
   }

function check_file
   {
   if [[ -f ${1} ]]
      then
	  echo "${1} exists, proceeding...."
   else
      echo "${1} not present, we're dying here...."
	  exit 10;
   fi
   }

function fin ()
   {
   echo "   Fini avec ${PROG}";
   echo "****************************************";
   echo " ";
   }

function dobrew ()
   {
   echo;
   if [[ ${1} ]]
      then
      echo "Installing ${1}";
      ${BREW} install ${1};
      #service=$(${BREW} install ${1} | grep 'sudo brew services');
      echo ${service}
      if [[ ${service} ]]
         then
         echo "Running ${service}";
         ${service}  || check_errm "***WARNING: $service failed" 55;
      else
         echo "nothing to do";
      fi
   fi
   }

function dobrewcask ()
   {
   echo;
   if [[ ${1} ]]
      then
      echo "Installing ${1}";
      ${BREW} cask install ${1} # || check_errm "***WARNING: ${1}  install failed" 55;
      # have to brute force privs, some pkgs write out stuff as root:admin
      sudo chown -R ${USER}:admin "/usr/local/";
   else
      echo "nothing to do";
   fi
   }




while getopts ":m:hv" Option
do
  case $Option in
    v  )
       echo ${version};;
    m  ) #argument required
       MANIFEST=${OPTARG};;
    h  ) # provide usage
       usage;;
    *  )
       echo "Unimplemented option chosen.";
       usage;
       ;;   # DEFAULT
   esac
done


if [[ ${1} == "-v" ]]
   then
   echo ${version};
   exit;
fi

if [[ ! ${MANIFEST} ]]
   then
   echo "No manifest specified, go with defaults ";
   MANIFEST="00default";
else
   echo "Install with ${MANIFEST}";
fi
echo;


if [[ ! -f  "~/.bash_profile" || ! $(grep '/usr/local/bin'  "~/.bash_profile") ]]
   then
   echo "Adding usr/local to .bash_profile";
   echo 'export PATH="/usr/local/bin:/usr/local/sbin:$PATH"' >> ~/.bash_profile
else
   echo "/usr/local already in .bash_profile";
fi
echo;

# Ask for the administrator password upfront.
echo "Authenticate and loop sudo once per minute until we're done";
echo;
sudo -v
# Keep-alive: update existing `sudo` time stamp until the script has finished.
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &


echo "Take control of /usr/local";
sudo chown -R ${USER}:admin "/usr/local/";
echo;

# Test for command line tools, if they are there install them
# these bits lifted from the brew installer
# brew would do this for us, but they ask for a keypress as tribute
if [[ -f "/System/Library/Receipts/com.apple.pkg.CLTools_Executables.plist" ]]
   then
   echo "command line tools found";
else
   echo "Installing command line tools";
   # install Xcode Command Line Tools
   #https://github.com/timsutton/osx-vm-templates/blob/
   #ce8df8a7468faa7c5312444ece1b977c1b2f77a4/scripts/xcode-cli-tools.sh
   touch /tmp/.com.apple.dt.CommandLineTools.installondemand.in-progress;
   PROD=$(softwareupdate -l | \
      grep "\*.*Command Line" | \
      head -n 1 | awk -F"*" '{print $2}' | \
      sed -e 's/^ *//' | \
      tr -d '\n')
   echo "softwareupdate -i $PROD";
   softwareupdate -i "$PROD" || check_errm "***WARNING: software update failed" 5;

   #   softwareupdate -i -r  || check_errm "***WARNING: software update failed" 5;
   #   echo "Installing command line tools, this will take a while";
   #   clt=$(softwareupdate -l | grep -B 1 -E "Command Line (Developer|Tools)" | \
   #   awk -F"*" '/^ +\\*/ {print $2}' | sed 's/^ *//' | tail -n1);
   #   softwareupdate -i "${clt}";
fi

if [[ -f ${BREW} ]]
   then
   echo
   echo "***Brew's already installed.";
else
   ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" || check_errm "Brew install failed" 45;

fi

echo "Run diagnostics JIC";
${BREW} doctor;

echo "Installing brew cask";
${BREW} tap caskroom/cask  || check_errm "***WARNING: brew tap cask install failed" 55;

echo "Installing manifests/${MANIFEST}";

while read line
  do
      dobrewcask ${line};
  done < "./manifests/${MANIFEST}/brew-casks.txt" | tee  ${BREWLOG};


while read line
  do
     dobrew ${line};
  done < "./manifests/${MANIFEST}/brew-list.txt" | tee  ${BREWLOG};

echo "Activating Services";
grep "sudo brew service" brew.log > services.tmp

while read line
  do
      ${line} || check_errm "***WARNING: ${line} failed" 55;
  done < "./services.tmp" | tee  ${BREWLOG};
rm "./services.tmp";



echo "Installing brew casks updater";
${BREW} tap buo/cask-upgrade || check_errm "***WARNING: buo/cask-upgrade install failed" 55;


echo "brew update, cleanup and cask cleanup"
${BREW} cleanup;
${BREW} cask cleanup;
${BREW} prune;

echo;
echo "    Signing off now";
exit 0;


